<?php

namespace Drupal\csv_export\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides form to export pupils.
 *
 * @package Drupal\csv_export\Form
 */
class ExportPupilsForm extends FormBase {

  /**
   * Current route match object.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $routeMatch;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->routeMatch = $container->get('current_route_match');

    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'csv_export';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['export_pupils'] = [
      '#type' => 'submit',
      '#value' => $this->t('Export invited pupils'),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $group = $this->routeMatch->getParameter('group');
    $members = $group->getMembers();
    $prepareCsv = "{$this->prepareCsvHeader($members)}\n";

    foreach ($members as $member) {
      $user = $member->getUser();
      $groupRole = $member->getRoles();
      $groupRole = reset($groupRole)->label();
      $created = date('d/m/Y', $member->getGroupContent()->getCreatedTime());
      $prepareCsv .= "{$user->field_esep_first_name->value},";
      $prepareCsv .= "{$user->field_esep_last_name->value},";
      $prepareCsv .= "{$user->label()},";
      $prepareCsv .= "$groupRole,";
      $prepareCsv .= "{$user->field_pupil_teacher_id->value},";
      $prepareCsv .= "$created\n";
    }

    $response = new Response($prepareCsv);
    $response->headers->set('Content-Type', 'text/csv');
    $response->headers->set('Content-Disposition', 'attachment;filename=' . 'exported_pupils.csv');

    $form_state->setResponse($response);
  }

  /**
   * Allows to prepare translatable csv headers instead of hardcoding them.
   *
   * @param array $members
   *   Group members.
   *
   * @return string
   *   String with translatable csv column headers.
   */
  public function prepareCsvHeader(array $members): string {
    $prepareCsv = '';
    // Loop through all members until member with all filled fields found.
    // We need member with all fields filled to get fields labels
    // and use them as CSV columns headers.
    // Then break the loop when member found.
    foreach ($members as $member) {
      $user = $member->getUser();
      $groupRole = $member->getRoles();
      $groupRole = reset($groupRole)->label();
      $firstName = $user->field_esep_first_name;
      $lastName = $user->field_esep_last_name;
      $teacherId = $user->field_pupil_teacher_id;
      $created = $member->getGroupContent()->getCreatedTime();
      if ($firstName->value &&
        $lastName->value &&
        $teacherId->value &&
        $groupRole &&
        $created &&
        $user->label()) {
        $prepareCsv .= "{$firstName->getFieldDefinition()->getLabel()},";
        $prepareCsv .= "{$lastName->getFieldDefinition()->getLabel()},";
        $prepareCsv .= "{$this->t("Username")},";
        $prepareCsv .= "{$this->t("Role")},";
        $prepareCsv .= $teacherId->getFieldDefinition()->getLabel();
        $prepareCsv .= "$created\n";

        // Break loop when all headers set.
        break;
      }
    }

    return $prepareCsv;
  }

}
