<?php

namespace Drupal\ajax_form\Element;

use Drupal\Core\Ajax\RemoveCommand;
use Drupal\file\Element\ManagedFile;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides widget for uploading and importing CSV files.
 *
 * @FormElement("import_csv")
 */
class CsvImport extends ManagedFile {

  /**
   * {@inheritdoc}
   */
  public static function processManagedFile(&$element, FormStateInterface $form_state, &$complete_form) {
    parent::processManagedFile($element, $form_state, $complete_form);

    $element['import_csv'] = [
      '#type' => 'button',
      '#value' => t('Import file'),
      '#attributes' => [
        'id' => 'import-file',
        'class' => ['import-button'],
      ],
    ];

    $element['remove_button']['#attributes']['class'][] = 'remove-button';

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function preRenderManagedFile($element) {
    $element = parent::preRenderManagedFile($element);

    if (empty($element['#value']['fids'])) {
      $element['import_csv']['#access'] = FALSE;
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function uploadAjaxCallback(&$form, FormStateInterface &$form_state, Request $request) {
    $renderer = \Drupal::service('renderer');

    $csv = $form['csv_pupils'];
    $renderer->renderRoot($csv);
    $response = parent::uploadAjaxCallback($form, $form_state, $request);

    $response->addCommand(new ReplaceCommand('#user-items', ''));
    $response->addCommand(new RemoveCommand('#edit-actions-create-user, #edit-actions-add-pupil, .sub-actions'));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public static function validateImportFile(&$element, FormStateInterface $form_state, &$complete_form) {
    $form_state->clearErrors();
  }

}
