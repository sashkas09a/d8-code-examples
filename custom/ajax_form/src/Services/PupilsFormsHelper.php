<?php

namespace Drupal\ajax_form\Services;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\Entity\User;

/**
 * Service provides helper methods for Pupils forms.
 *
 * @package Drupal\ajax_form\Services
 */
class PupilsFormsHelper {

  use StringTranslationTrait;

  /**
   * Manages entity types.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  public $entityTypeManager;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  public $currentUser;

  /**
   * Group role storage.
   *
   * @var \Drupal\group\Entity\Storage\GroupRoleStorage
   */
  protected $groupRole;

  /**
   * User storage.
   *
   * @var \Drupal\user\UserStorage
   */
  public $user;

  /**
   * Construct PupilsFormsHelper service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Session\AccountProxy $currentUser
   *   Current user.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManager $entityTypeManager, AccountProxy $currentUser) {
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
    $this->groupRole = $entityTypeManager->getStorage('group_role');
    $this->user = $entityTypeManager->getStorage('user');
  }

  /**
   * Get roles for specific group.
   *
   * @param string $groupType
   *   Group type id.
   *
   * @return array
   *   Array of group roles.
   */
  public function getGroupRoles(string $groupType):array {
    $roleOptions = [];
    // Get roles available in group.
    $group_roles = $this->groupRole->loadByProperties([
      'group_type' => $groupType,
      'internal' => FALSE,
    ]);
    foreach ($group_roles as $roleId => $role) {
      $roleOptions[$roleId] = $role->label();
    }

    return $roleOptions;
  }

  /**
   * Generate form.
   *
   * @param array $form
   *   Array contains form information.
   * @param array $rowsArray
   *   Array with rows and their data.
   */
  public function generateForm(array &$form, array $rowsArray): void {
    $form['#tree'] = TRUE;

    $form['sub_actions'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['sub-actions'],
      ],
    ];

    $form['sub_actions']['generate_pass'] = [
      '#type' => 'button',
      '#value' => $this->t('Generate passwords'),
      '#attributes' => [
        'id' => 'generate-pass',
      ],
    ];

    $form['sub_actions']['show_pass'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show passwords'),
      '#attributes' => [
        'class' => ['show-pass'],
      ],
    ];

    $form['user_items'] = [
      '#type' => 'table',
      '#attributes' => [
        'id' => 'user-items',
        'class' => ['user-items'],
      ],
      '#header' => [
        $this->t('First name'),
        $this->t('Last name'),
        $this->t('Username'),
        $this->t('Password'),
        $this->t('Role'),
        $this->t('Action'),
      ],
    ];

    $this->generateUserRows(
      $form,
      ['first_name', 'last_name', 'username', 'pass'],
      $rowsArray
    );

    $form['actions']['add_pupil'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add pupil'),
      '#submit' => ['::addItem'],
      '#weight' => 10,
      '#ajax' => [
        'event' => 'click',
        'callback' => '::addUserItem',
        'wrapper' => 'user-items',
        'progress' => FALSE,
      ],
    ];

    $form['actions']['create_user'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add pupils to Twinspace'),
    ];
  }

  /**
   * Generate number of user rows.
   *
   * Same as number of elements in $rowsArray array.
   *
   * @param array $form
   *   Array contains form information.
   * @param array $elements
   *   Names of elements to generate.
   *   For example ['first_name', 'last_name', 'username'].
   * @param array $rowsArray
   *   Array with rows and their data.
   */
  public function generateUserRows(array &$form, array $elements, array $rowsArray): void {
    // Generate row per each array element.
    foreach ($rowsArray as $i => $userData) {
      $form['user_items']['user_item' . $i] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['user-item'],
        ],
      ];

      // Build form elements.
      $this->buildElements($form, $elements, $userData, $i);

      // Add Role select to each row.
      $form['user_items']['user_item' . $i]['role'] = [
        '#type' => 'select',
        '#options' => $this->getGroupRoles('esep_twinspace'),
      ];

      // Don't generate 'Remove' button if only one user_item generated.
      if (count($rowsArray) > 1) {
        $form['user_items']['user_item' . $i]['remove'] = [
          '#type' => 'submit',
          '#value' => $this->t('Remove'),
          '#submit' => ['::removeItem'],
          '#name' => 'op' . $i,
          '#ajax' => [
            'event' => 'click',
            'callback' => '::removeUserItem',
            'wrapper' => 'user-items',
            'progress' => FALSE,
          ],
        ];
      }

      // Add AJAX callbacks.
      $form['user_items']['user_item' . $i]['first_name']['#ajax']['callback'] = '::generateFromFirstLastNames';
      $form['user_items']['user_item' . $i]['last_name']['#ajax']['callback'] = '::generateFromFirstLastNames';
      $form['user_items']['user_item' . $i]['username']['#ajax']['callback'] = '::checkUsernameInput';
    }
  }

  /**
   * Build similar textfields.
   *
   * @param array $form
   *   Array contains form information.
   * @param array $elements
   *   Elements names for build.
   * @param array|int $userData
   *   Array contains users data or contains row number if not data provided.
   * @param int|string $number
   *   Element number or element name.
   */
  public function buildElements(array &$form, array $elements, $userData, $number): void {
    foreach ($elements as $key => $element) {
      $form['user_items']['user_item' . $number][$element] = [
        '#type' => 'textfield',
        '#size' => 20,
        '#required' => TRUE,
        '#default_value' => $userData[$key],
        '#attributes' => [
          'class' => [str_replace('_', '-', $element)],
        ],
      ];
      if ($element === 'username' && !empty($userData[$key])) {
        $pupil = $this->user->loadByProperties(['name' => $userData[$key]]);
        $pupil = reset($pupil);
        if ($pupil && !$this->checkPupil($pupil)) {
          $generatedUsername = $this->generateUsername($userData[$key]);
          $warning = $this->usernameWarning($userData[$key], $generatedUsername);
          $form['user_items']['user_item' . $number]['#description'] = $warning;
        }
      }
      // Add AJAX to all inputs except password input.
      if ($element !== 'pass') {
        $form['user_items']['user_item' . $number][$element]['#ajax'] = [
          'event' => 'change',
          'wrapper' => 'user-items',
          'disable-refocus' => TRUE,
          'progress' => FALSE,
        ];
      }

    }
  }

  /**
   * Provides warning message.
   *
   * @param string $username
   *   Username.
   * @param string $generatedUsername
   *   Generated username.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   Markup object that contains error text.
   */
  public function usernameWarning(string $username, string $generatedUsername):TranslatableMarkup {
    return $this->t(
      'Username @username from your CSV import already exists.
      Propose to use @generated_username as username', [
        '@username' => $username,
        '@generated_username' => $generatedUsername,
      ]);
  }

  /**
   * Checks that current user is teacher for provided pupil.
   *
   * @param \Drupal\user\Entity\User $pupil
   *   User object.
   *
   * @return bool
   *   TRUE if current user is teacher for provided pupil. FALSE otherwise.
   */
  public function checkPupil(User $pupil):bool {
    // Get pupil teacher.
    $teacher = $pupil->get('field_pupil_teacher_id')->getValue()[0]['value'] ?? NULL;

    // TRUE if teacher id equals current user id.
    return $teacher === $this->currentUser->id();
  }

  /**
   * Generates username.
   *
   * @param string $username
   *   Username.
   *
   * @return string
   *   Generated username.
   */
  public function generateUsername(string $username):string {
    $i = 1;
    while ($user = $this->user->loadByProperties(['name' => $username])) {
      $username = "$username.$i";
      $i++;
    }

    return $username;
  }

}
