<?php

namespace Drupal\ajax_form\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides form to import pupils.
 *
 * @package Drupal\ajax_form\Form
 */
class ImportPupilsForm extends AddNewPupilsForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'esep_import_pupils_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['import_description'] = [
      '#type' => 'item',
      '#markup' => $this->t('Please insert in your document the following values for pupils: 
        First name, Last name, Password 
        (can also be generated automatically after upload) and Role 
        (member or administrator).') . '<a href="#">' . $this->t('View example') . '</a>',
      '#attributes' => [
        'class' => ['import-description'],
      ],
    ];
    $this->addCsvUploadButton($form, $form_state);

    $triggeredParents = $form_state->getTriggeringElement()['#parents'] ?? NULL;

    // Provide logic depends on which AJAX element triggered.
    if ($triggeredParents) {
      $this->triggeredElementLogic($form, $form_state, $triggeredParents);
    }

    $form['#attached']['library'][] = 'ajax_form/pupils_creation';

    return $form;
  }

  /**
   * Add logic to triggered elements.
   *
   * @param array $form
   *   Array contains form information.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   * @param array $triggeredParents
   *   Array with parents of triggered element.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function triggeredElementLogic(array &$form, FormStateInterface $form_state, array $triggeredParents): void {
    if ($this->checkTriggered($triggeredParents, 'import_csv')) {
      $this->rowsArray = [];
      $fid = $form_state->getValue('csv_pupils');
      $file = $this->pupilsFormsHelper->entityTypeManager->getStorage('file')->load($fid[0]);
      if ($file) {
        $data = fopen($file->getFileUri(), 'rb');
        while ($row = fgetcsv($data)) {
          $this->rowsArray[] = $row;
        }
        $this->pupilsFormsHelper->generateForm($form, $this->rowsArray);
      }

      $input = $form_state->getUserInput();
      $input['csv_pupils'] = NULL;
      $form_state->setUserInput($input);
    }

    if ($this->checkTriggered($triggeredParents, 'upload_button')) {
      $this->addCsvUploadButton($form, $form_state);
    }

    if ($this->checkTriggered($triggeredParents, 'user_items') ||
      $this->checkTriggered($triggeredParents, 'add_pupil')) {
      $this->pupilsFormsHelper->generateForm($form, $this->rowsArray);
    }
  }

  /**
   * Check if element exist in triggered parents array.
   *
   * @param array $triggeredParents
   *   Array with parents of triggered element.
   * @param string $key
   *   Key to search.
   *
   * @return bool
   *   TRUE if $key found in $triggeredParents. FALSE otherwise.
   */
  public function checkTriggered(array $triggeredParents, string $key): bool {
    return $triggeredParents && in_array($key, $triggeredParents);
  }

  /**
   * Provide CSV upload button element.
   *
   * @param array $form
   *   Array contains form information.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   */
  public function addCsvUploadButton(array &$form, FormStateInterface $form_state): void {
    $form['csv_pupils'] = [
      '#title' => $this->t('Drag files here to upload'),
      '#type' => 'import_csv',
      '#upload_location' => 'private://csv_pupils/',
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
      ],
      '#description' => $this->t('You can upload CSV, XLS and XLSX files'),
      '#attributes' => [
        'class' => ['csv-pupils'],
      ],
    ];
  }

  /**
   * Create drupal users and add them to group.
   *
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->get(UserPasswordKeyConfigForm::SETTINGS);

    $userItems = $form_state->getValue('user_items');
    $group = $this->request->getCurrentRequest()->get('group');
    foreach ($userItems as $userItem) {
      $user = $this->pupilsFormsHelper->user->loadByProperties(['name' => $userItem['username']]);
      $user = reset($user);
      // Associate user with current teacher if user exist
      // and add user to Twinspace.
      if ($user) {
        $this->addUserToGroupAndTeacher($group, $user, $userItem['role']);
      }
      // Create user if it not exist yet.
      else {
        $user = $this->createUser($userItem, $config);
        $this->associateUserAndTeacher($user);
        // Add user to group(Twinspace).
        if ($user && $group) {
          $this->addUserToGroup($group, $user, $userItem['role']);
        }
      }
    }
  }

}
