<?php

namespace Drupal\ajax_form\Form;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides form to create new pupils users.
 *
 * @package Drupal\ajax_form\Form
 */
class AddNewPupilsForm extends EditExistingPupilsForm {

  /**
   * Group storage.
   *
   * @var \Drupal\Core\Entity\Sql\SqlContentEntityStorage
   */
  protected $group;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);

    $entityManager = $container->get('entity_type.manager');
    $instance->group = $entityManager->getStorage('group');
    $instance->configFactory = $container->get('config.factory');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'esep_add_new_pupils_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->pupilsFormsHelper->generateForm($form, $this->rowsArray);

    $form['#attached']['library'][] = 'ajax_form/pupils_creation';

    return $form;
  }

  /**
   * AJAX callback for 'First name' and 'Last name' inputs.
   *
   * Generate username from first name and last name fields.
   *
   * @param array $form
   *   Array contains form information.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   *
   * @return array
   *   User items container.
   */
  public function generateFromFirstLastNames(array $form, FormStateInterface $form_state):array {
    $position = $this->getTriggeredRow($form_state);
    $firstName = $form_state->getValue(
      ['user_items', "user_item$position", 'first_name']
    );
    $lastName = $form_state->getValue(
      ['user_items', "user_item$position", 'last_name']
    );
    $username = "$firstName.$lastName";
    $usernameCopy = $username;
    $usernames = array_column($form_state->getValue('user_items'), 'username');

    $i = 1;
    while (in_array($username, $usernames, FALSE) ||
      $user = $this->pupilsFormsHelper->user->loadByProperties(['name' => $username])) {
      $username = "$usernameCopy.$i";
      $i++;
    }

    $form['user_items']['user_item' . $position]['username']['#value'] = $username;

    return $form['user_items'];
  }

  /**
   * AJAX callback for 'Username' input.
   *
   * Check username in 'Username' input
   * and generate new one if username already exist in database
   * or if username exist in another 'Username' form inputs.
   *
   * @param array $form
   *   Array contains form information.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   *
   * @return array
   *   User items container.
   */
  public function checkUsernameInput(array $form, FormStateInterface $form_state):array {
    $position = $this->getTriggeredRow($form_state);
    $username = $form_state->getValue([
      'user_items', "user_item$position", 'username',
    ]);
    $usernames = array_column($form_state->getValue('user_items'), 'username');

    // Remove current focused input from checking array.
    unset($usernames[$position]);

    // Check that username input is not empty.
    if ($username) {
      $i = 1;
      while (in_array($username, $usernames, FALSE) ||
        $user = $this->pupilsFormsHelper->user->loadByProperties(['name' => $username])) {
        $username = "$username.$i";
        $i++;
      }

      $form['user_items']['user_item' . $position]['username']['#value'] = $username;
    }

    return $form['user_items'];
  }

  /**
   * Encrypts provided string.
   *
   * @param string $str
   *   String that should be encrypted.
   * @param string $key
   *   Encryption key.
   * @param string $cipher
   *   Encryption method.
   *
   * @return string
   *   Encrypted string.
   */
  public function encryptString(string $str, string $key, string $cipher):string {
    $cipherMethods = openssl_get_cipher_methods();
    $cipher = in_array($cipher, $cipherMethods) ? $cipher : reset($cipherMethods);

    return openssl_encrypt($str, $cipher, $key);
  }

  /**
   * Create drupal users and add them to group.
   *
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->get(UserPasswordKeyConfigForm::SETTINGS);
    $userItems = $form_state->getValue('user_items');
    /** @var \Drupal\group\Entity\Group $group */
    $group = $this->request->getCurrentRequest()->get('group');
    foreach ($userItems as $userItem) {
      $user = $this->createUser($userItem, $config);
      $this->addUserToGroupAndTeacher($group, $user, $userItem['role']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $triggeredRow = $this->getTriggeredRow($form_state);
    $triggered = $form_state->getTriggeringElement();
    // Not put validation errors if Add Pupil or Remove buttons clicked.
    if (in_array('add_pupil', $triggered['#parents']) ||
      in_array('remove', $triggered['#parents'])) {
      $form_state->clearErrors();
    }
    // Add validation to username.
    // When final form submit button('Add Pupils to Twinspace') clicked.
    if (in_array('create_user', $triggered['#parents'])) {
      $usernames = $usernames = array_column($form_state->getValue('user_items'), 'username');
      foreach ($usernames as $username) {
        $user = $this->pupilsFormsHelper->user->loadByProperties(['name' => $username]);
        $user = reset($user);
        // Set warning if pupil with $username already exists
        // And pupil teacher has same id as current teacher(current user).
        if ($user && !$this->pupilsFormsHelper->checkPupil($user)) {
          $generatedUsername = $this->pupilsFormsHelper->generateUsername($username);
          $form_state
            ->setErrorByName(
              "user_items][user_item$triggeredRow][username",
              $this->pupilsFormsHelper->usernameWarning($username, $generatedUsername)
            );
        }
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * Creates user.
   *
   * @param array $userItem
   *   Array with user data.
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   Config object.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   User object.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createUser(array $userItem, ImmutableConfig $config) {
    $user = $this->pupilsFormsHelper->user->create();
    $user->set('field_esep_first_name', $userItem['first_name']);
    $user->set('field_esep_last_name', $userItem['last_name']);
    $user->set(
      'field_esep_crypt_pass',
      $this->encryptString($userItem['pass'], $config->get('pass_key'), 'aes-128-ecb')
    );
    $user->setUsername($userItem['username']);
    $user->setPassword($userItem['pass']);
    $user->addRole('esep_pupil');
    $user->activate();
    $user->save();

    return $user;
  }

}
