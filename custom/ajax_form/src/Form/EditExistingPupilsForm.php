<?php

namespace Drupal\ajax_form\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\group\Entity\GroupInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides form to edit existing pupils.
 *
 * @package Drupal\ajax_form\Form
 */
class EditExistingPupilsForm extends FormBase {

  /**
   * Array contains user rows.
   *
   * Array increased/decreased when user item add to or remove from form.
   *
   * @var array
   */
  public $rowsArray = [0 => 0];

  /**
   * Pupils forms helper service.
   *
   * @var \Drupal\ajax_form\Services\PupilsFormsHelper
   */
  protected $pupilsFormsHelper;

  /**
   * Request.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack|null
   */
  protected $request;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->pupilsFormsHelper = $container->get('ajax_form.pupils_forms_helper');
    $instance->request = $container->get('request_stack');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'esep_edit_existing_pupils_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#tree'] = TRUE;

    $form['user_items'] = [
      '#type' => 'table',
      '#attributes' => [
        'id' => 'user-items',
        'class' => ['user-items'],
      ],
      '#header' => [
        $this->t('Username'),
        $this->t('Role'),
        $this->t('Action'),
      ],
    ];

    $this->pupilsFormsHelper->generateUserRows($form, ['username'], $this->rowsArray);

    $form['actions']['add_pupil'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add pupil'),
      '#submit' => ['::addItem'],
      '#weight' => 10,
      '#ajax' => [
        'event' => 'click',
        'callback' => '::addUserItem',
        'wrapper' => 'user-items',
        'progress' => FALSE,
      ],
    ];

    $form['actions']['create_user'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add pupils to Twinspace'),
    ];

    $form['#attached']['library'][] = 'ajax_form/pupils_creation';

    return $form;
  }

  /**
   * Get row number of triggered element.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   *
   * @return string
   *   String with row number.
   *   If 'user_item2' return '2', if 'user_item7' return '7'.
   */
  public function getTriggeredRow(FormStateInterface $form_state):string {
    $triggered = $form_state->getTriggeringElement();
    $parent = preg_grep('/user_item[0-9]+$/', $triggered['#array_parents']);

    return trim(reset($parent), 'user_item');
  }

  /**
   * Adds value to $rowsArray array.
   *
   * Array will be used to create user rows.
   * Quantity of user rows equals to number of elements in this array.
   *
   * @param array $form
   *   Array contains form information.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   */
  public function addItem(array $form, FormStateInterface $form_state): void {
    $end = array_key_last($this->rowsArray);
    $this->rowsArray[$end + 1] = $end + 1;
    $form_state->setRebuild();
  }

  /**
   * Remove element from $rowsArray array according to the clicked button.
   *
   * $rowsArray array reduces to one value.
   * So one less user row will be build on form render.
   *
   * @param array $form
   *   Array contains form information.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   */
  public function removeItem(array $form, FormStateInterface $form_state): void {
    $position = $this->getTriggeredRow($form_state);
    unset($this->rowsArray[$position]);
    $form_state->setRebuild();
  }

  /**
   * AJAX callback for 'Add pupil' button.
   *
   * Add user row if 'Add pupil' button clicked.
   *
   * @param array $form
   *   Array contains form information.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   *
   * @return array
   *   User items container.
   */
  public function addUserItem(array $form, FormStateInterface $form_state):array {
    return $form['user_items'];
  }

  /**
   * AJAX callback for 'Remove' button.
   *
   * Remove user row if 'Remove' button clicked.
   *
   * @param array $form
   *   Array contains form information.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   *
   * @return array
   *   User items container.
   */
  public function removeUserItem(array $form, FormStateInterface $form_state):array {
    return $form['user_items'];
  }

  /**
   * Create drupal users and add them to group.
   *
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $userItems = $form_state->getValue('user_items');
    $group = $this->request->getCurrentRequest()->get('group');
    foreach ($userItems as $userItem) {
      $user = user_load_by_name($userItem);
      if ($group && $user) {
        // Associate user with current teacher if user exist
        // and add user to Twinspace.
        $this->addUserToGroupAndTeacher($group, $user, $userItem['role']);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $triggered = $form_state->getTriggeringElement();
    if (in_array('add_pupil', $triggered['#parents']) ||
      in_array('remove', $triggered['#parents'])) {
      $form_state->clearErrors();
    }
    // Check if user exist when 'Add to Twinspace' button clicked.
    if (in_array('create_user', $triggered['#parents'])) {
      $usernames = array_column($form_state->getValue('user_items'), 'username');
      foreach ($usernames as $username) {
        $user = $this->pupilsFormsHelper->user->loadByProperties(['name' => $username]);
        $user = reset($user);
        // Set error if pupil with $username doesn't exists.
        if (!$user) {
          $triggeredRow = $this->getTriggeredRow($form_state);
          $form_state
            ->setErrorByName(
              "user_items][user_item$triggeredRow][username",
              $this->userNotExists($username)
            );
        }
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * Prepares warning.
   *
   * @param string $username
   *   Provided username.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   Warning message.
   */
  public function userNotExists(string $username): TranslatableMarkup {
    return $this->t(
      "User with @username doesn't exists.
      You can add new user to your Twinspace on Add pupils page", [
        '@username' => $username,
      ]
    );
  }

  /**
   * Associate user with group and teacher.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   Gropu object.
   * @param \Drupal\user\UserInterface $user
   *   User object.
   * @param string $groupRole
   *   Role id.
   */
  public function addUserToGroupAndTeacher(GroupInterface $group, UserInterface $user, string $groupRole): void {
    if ($user && $group) {
      $this->addUserToGroup($group, $user, $groupRole);
      $this->associateUserAndTeacher($user);
    }
  }

  /**
   * Associate user with group.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   Gropu object.
   * @param \Drupal\user\UserInterface $user
   *   User object.
   * @param string $groupRole
   *   Role id.
   */
  public function addUserToGroup(GroupInterface $group, UserInterface $user, string $groupRole): void {
    $group->addMember($user, ['group_roles' => [$groupRole]]);
    $group->save();
  }

  /**
   * Associate user with teacher.
   *
   * @param \Drupal\user\UserInterface $user
   *   User object.
   */
  public function associateUserAndTeacher(UserInterface $user): void {
    $user->set('field_pupil_teacher_id', $this->pupilsFormsHelper->currentUser->id());
    $user->save();
  }

}
