<?php

namespace Drupal\ajax_form\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides form to set user password encrypt key.
 *
 * @package Drupal\ajax_form\Form
 */
class UserPasswordKeyConfigForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'ajax_form.pass_encrypt_key';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ecnrypt_user_pass_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['pass_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password Encrypt Key'),
      '#default_value' => $config->get('pass_key'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('pass_key', $form_state->getValue('pass_key'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
