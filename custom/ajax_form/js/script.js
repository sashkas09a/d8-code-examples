(function ($, Drupal) {
  Drupal.behaviors.pupilsManuallyCreation = {
    attach: function (context, settings) {

      let a;
      $('input').once().focus(function(e) {
        a = $(this).attr('name');
      });

      $(document, context).ajaxSuccess(function(event, xhr, settings) {
        let inputName = settings.extraData._triggering_element_name.match(/\[([a-zA-Z0-9_-]+)\]$/);
        if (inputName) {
          let input = $('[name=' + '"' + a + '"' + ']');
          input.focus();
          let tmpStr = input.val();
          input.val('');
          input.val(tmpStr);
        }
      });

      let pupilPass = $('.pass', context);

      // Change password input type to 'password'.
      pupilPass.each(function (){
        $(this).attr('type', 'password');
      });

      // Show passwords when checkbox checked.
      $('#edit-sub-actions-show-pass', context).click(function (){
        let type = 'password';
        if (this.checked) {
          type = 'text'
        }
        pupilPass.each(function (){
          $(this).attr('type', type);
        });
      });

      // Generate 6-digit pupils passwords.
      $('#generate-pass', context).once().click(function (e){
        $('.pass', context).each(function (){
          if ($(this).val().length < 6) {
            $(this).val(generatePass(6));
          }
        });
        e.preventDefault();
      });

      // Generate passwords.
      function generatePass(len, charSet) {
        charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let randomString = '';
        for (let i = 0; i < len; i++) {
          let randomPoz = Math.floor(Math.random() * charSet.length);
          randomString += charSet.substring(randomPoz,randomPoz+1);
        }
        return randomString;
      }

    }
  };
})(jQuery, Drupal);
