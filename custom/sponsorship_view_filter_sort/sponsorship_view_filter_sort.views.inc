<?php

/**
 * @file
 * File to define view plugins.
 */

/**
 * Implements hook_views_data_alter().
 *
 * Used to define sponsor_sort sort plugin.
 * Used to define primary_categories_child_terms filter plugin.
 */
function sponsorship_view_filter_sort_views_data_alter(array &$data) {
  $data['node__field_sponsorship']['sponsor_sort'] = [
    'title' => t('Sponsor:term id'),
    'group' => t('Content'),
    'help' => t('Sort companies by sponsor term id according to current term. Pulling companies that have sponsorship on current page to top of company list.'),
    'sort' => [
      'field' => 'field_sponsorship',
      'id' => 'sponsor_sort',
    ],
  ];

  $data['taxonomy_term_field_data']['primary_categories_child_terms'] = [
    'title' => t('Primary categories child terms'),
    'group' => t('Taxonomy term'),
    'filter' => [
      'title' => t('Primary categories child terms'),
      'help' => t('Only child terms of term which selected in expose filter, from primary_categories vocabulary.'),
      'field' => 'tid',
      'id' => 'primary_categories_child_terms',
    ],
  ];
}
