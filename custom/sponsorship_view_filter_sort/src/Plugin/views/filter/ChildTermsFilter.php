<?php

namespace Drupal\sponsorship_view_filter_sort\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\StringFilter;

/**
 * Filtering by child terms based on term which was selected in filter form.
 *
 * @ViewsFilter("primary_categories_child_terms")
 */
class ChildTermsFilter extends StringFilter {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();

    $category = $this->value;
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('primary_categories', $category);
    foreach ($terms as $term) {
      $tids[] = $term->tid;
    }

    $this->query->addWhere('0', 'taxonomy_term_field_data.tid', $tids, 'IN');
  }

}
