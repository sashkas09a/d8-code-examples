<?php

namespace Drupal\plugin_type_manager;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the City widget plugin manager.
 */
class EntitiesByTitlesPluginManager extends DefaultPluginManager {

  /**
   * Constructs a new CityWidgetManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/EntitiesByTitles',
      $namespaces,
      $module_handler,
      'Drupal\plugin_type_manager\EntitiesByTitlesPluginInterface',
      'Drupal\plugin_type_manager\Annotation\EntitiesByTitles');

    $this->alterInfo('plugin_type_manager_info');
    $this->setCacheBackend($cache_backend, 'plugin_type_manager');
  }

}
