<?php

namespace Drupal\plugin_type_manager\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\user\Entity\User;

/**
 * Provides a block to display node titles.
 *
 * @Block(
 *  id = "node_titles",
 *  admin_label = @Translation("Node titles Block"),
 *  category = @Translation("Node titles"),
 *  deriver = "Drupal\node_by_title_letter_derivatives\Plugin\Derivative\NodeTitlesRelatedToLetter"
 * )
 */
class NodeTitlesRelatedToLetterBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $markup = '';
    foreach ($this->getPluginDefinition()['results'] as $nid => $title) {
      $url = new Url('entity.node.canonical', ['node' => $nid]);
      $link = new Link($title, $url);
      $link = $link->toString();
      $markup .= $link . "</br>";
    }

    return [
      '#markup' => $markup,
    ];
  }

}
