<?php

namespace Drupal\plugin_type_manager\Plugin\EntitiesByTitles;

use Drupal\plugin_type_manager\Annotation\EntitiesByTitles;
use Drupal\plugin_type_manager\EntitiesByTitlesPluginBase;

/**
 * @EntitiesByTitles(
 *   id = "nodes_with_a_letter",
 *   label = @Translation("Nodes that starts with 'A' letter configured by admin"),
 * )
 * @package Drupal\plugin_type_manager\Plugin\EntitiesByTitles
 */
class NodesLetterRelatedTitles extends EntitiesByTitlesPluginBase {

  /**
   * Get nodes which starts from the specific letter.
   *
   * From letter 'A' in current case.
   *
   * @return array
   *   Return array of nodes ids.
   */
  public function getEntities() {

    $query = $this->database->select('node_field_data', 'nfd');
    $query->condition('nfd.title', 'A%', 'LIKE')
      ->fields('nfd', ['nid']);
    $result = $query->execute();
    $result = $result->fetchCol();

    return $result ?: [];
  }

}
