<?php

namespace Drupal\plugin_type_manager\Plugin\EntitiesByTitles;

use Drupal\plugin_type_manager\EntitiesByTitlesPluginBase;
use Drupal\plugin_type_manager\Annotation\EntitiesByTitles;
use Drupal\node\NodeInterface;

/**
 * @EntitiesByTitles(
 *   id = "nodes_with_related_titles",
 *   label = @Translation("Node starts that start with same letter that current node"),
 * )
 */
class CurrentNodeRelatedTitles extends EntitiesByTitlesPluginBase {

  /**
   * Get nodes which starts from the letter that current node start.
   *
   * For example, if current node title starts from 's',
   * so only nodes which title starts from 's' will be retrieved.
   *
   * @return array
   *   Return array of nodes ids.
   */
  public function getEntities() {
    $letter = NULL;

    if ($this->configuration['node'] instanceof NodeInterface) {
      $letter = substr($this->configuration['node']->getTitle(), 0, 1);
    }

    $query = $this->database->select('node_field_data', 'nfd');
    $query->condition('nfd.title', $letter . '%', $letter ? 'LIKE' : 'NOT IN')
      ->fields('nfd', ['nid'])
      ->range(0, $this->configuration['length'] ?? NULL);
    $result = $query->execute();
    $result = $result->fetchCol();

    return $result ?: [];
  }

}
