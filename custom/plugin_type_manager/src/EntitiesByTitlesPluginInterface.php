<?php

namespace Drupal\plugin_type_manager;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Provides Drupal/plugin_type_manager/EntitiesByTitlesPluginInterface.
 */
interface EntitiesByTitlesPluginInterface extends ContainerFactoryPluginInterface {

  /**
   * Get entities ids.
   *
   * @return array
   *   Array of node ids.
   */
  public function getEntities();

}
