<?php

namespace Drupal\plugin_type_manager\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a EntitiesByTitles annotation object.
 *
 * @Annotation
 */
class EntitiesByTitles extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

}
