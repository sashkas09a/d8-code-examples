<?php


namespace Drupal\plugin_type_manager;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base plugin class to get entities by title of required type.
 */
abstract class EntitiesByTitlesPluginBase extends PluginBase implements EntitiesByTitlesPluginInterface {

  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * EntitiesByTitlesPluginBase constructor.
   *
   * @param array $configuration
   *   Set of configurations.
   * @param string|int $plugin_id
   *   Plugin id.
   * @param array $plugin_definition
   *   Plugin definition.
   * @param \Drupal\Core\Database\Connection $database
   *   Database connection.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, Connection $database) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database')
    );
  }

  /**
   * Provides required entities ids.
   *
   * User is free to put any logic to get entities.
   *
   * @return array
   *   Array of entities ids.
   */
  public function getEntities() {
    return [];
  }

}
