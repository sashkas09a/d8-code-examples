<?php

namespace Drupal\node_by_title_letter_derivatives\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides derivative based on node titles and bunch of letters.
 */
class NodeTitlesRelatedToLetter extends DeriverBase implements ContainerDeriverInterface {

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * NodeTitlesRelatedToLetter constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static($container->get('database'));
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $letters = ['a', 'c', 'o'];

    foreach ($letters as $letter) {
      $query = $this->database->select('node_field_data', 'nfd');
      $query->condition('nfd.title', $letter . '%', $letter ? 'LIKE' : 'NOT IN')
        ->fields('nfd', ['nid', 'title']);
      $result = $query->execute();
      $result = $result->fetchAllKeyed();
      $this->derivatives[$letter] = $base_plugin_definition;
      $this->derivatives[$letter]['admin_label'] = 'Nodes start with "' . $letter . '"';
      $this->derivatives[$letter]['results'] = $result;
    }

    return $this->derivatives;
  }

}
