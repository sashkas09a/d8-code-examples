<?php

/**
 * @file
 * Contains the code to generate the custom drush commands.
 */

/**
 * Implements hook_drush_command().
 */
function drush_batch_command_drush_command() {
  $items = [];
  $items['nodes-update'] = [
    'description' => 'Generate batch to update nodes fields',
    'aliases' => ['n-upd'],
  ];
  return $items;
}

/**
 * Drush command callback.
 */
function drush_drush_batch_command_nodes_update() {
  $connection = \Drupal::database();
  $number = count($connection->select('node__field_pp_company_description_tm', 'f')
    ->fields('f', ['field_pp_company_description_tm_value', 'entity_id'])
    ->execute()->fetchAll());
  $operations[] = ['node_update', [$number]];
  $batch = [
    'init_message' => t('Executing a batch...'),
    'operations' => $operations,
    'file' => drupal_get_path('module', 'general') . '/general.batch.inc',
  ];
  batch_set($batch);
  batch_process();
  drush_print('Nodes start to update >>>');
  drush_invoke('batch-process', batch_get()['id']);
}
