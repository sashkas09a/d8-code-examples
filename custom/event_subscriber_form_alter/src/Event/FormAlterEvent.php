<?php


namespace Drupal\event_subscriber_form_alter\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event that is fired when form alters.
 */
class FormAlterEvent extends Event {

  /**
   * Name of the event fired when a hook_form_alter called to login form.
   *
   * @Event
   * @var string
   */
  const LOGIN_FORM_ALTER = 'event_subscriber_login_form_alter';

  /**
   * Name of the event fired when a hook_form_alter called to login form.
   *
   * @Event
   * @var string
   */
  const REGISTRATION_FORM_ALTER = 'event_subscriber_registration_form_alter';

  /**
   * Form array.
   *
   * @var array
   */
  public $form;

  /**
   * Construct FormAlter event.
   *
   * @param array $form
   *   Array contains form information.
   */
  public function __construct(array &$form) {
    $this->form = &$form;
  }

}
