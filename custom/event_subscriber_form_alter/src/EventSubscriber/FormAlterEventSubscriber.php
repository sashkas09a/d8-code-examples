<?php


namespace Drupal\event_subscriber_form_alter\EventSubscriber;

use Drupal\event_subscriber_form_alter\Event\FormAlterEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscriber events class.
 *
 * @package Drupal\event_subscriber_form_alter\EventSubscriber
 */
class FormAlterEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      FormAlterEvent::LOGIN_FORM_ALTER => 'loginFormAlter',
      FormAlterEvent::REGISTRATION_FORM_ALTER => 'registrationFormAlter',
    ];
  }

  /**
   * Subscribe to the login form alter event.
   *
   * @param \Drupal\event_subscriber_form_alter\Event\FormAlterEvent $event
   *   Login form alter event.
   */
  public function loginFormAlter(FormAlterEvent $event) {
    $event->form['name']['#title'] = t('Login');
  }

  /**
   * Subscribe to the registration form alter event.
   *
   * @param \Drupal\event_subscriber_form_alter\Event\FormAlterEvent $event
   *   Registration form alter event.
   */
  public function registrationFormAlter(FormAlterEvent $event) {
    $event->form['account']['mail']['#title'] = t('Email');
  }

}
