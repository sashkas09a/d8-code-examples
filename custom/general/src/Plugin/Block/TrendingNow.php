<?php

namespace Drupal\general\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Provides a 'Trending now' block.
 *
 * @Block(
 *   id = "trending_now",
 *   admin_label = @Translation("Trending now"),
 * )
 */
class TrendingNow extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Creates an SponsorFilterField object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $connection
   *   The views plugin manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    Connection $connection,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->connection = $connection;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $queryArticles = $this->connection->select('node_field_data', 'n');
    $queryArticles->addField('n', 'nid');
    $queryArticles->condition('n.type', 'article');
    $queryArticles->condition('n.status', 1);
    $queryArticles->orderBy('n.created', 'DESC');
    $queryArticles->range(0, 2);
    $resultArticles = $queryArticles->execute()->fetchAll();

    $queryTerms = $this->connection->select('taxonomy_term_data', 't');
    $queryTerms->join('taxonomy_publish', 'tb', 'tb.tid = t.tid');
    $queryTerms->addField('t', 'tid');
    $queryTerms->condition('t.vid', 'primary_categories');
    $queryTerms->condition('tb.status', 1);
    $queryTerms->orderRandom();
    $queryTerms->range(0, 2);
    $resultTerms = $queryTerms->execute()->fetchAll();

    foreach ($resultArticles as $key => $value) {
      $nids[] = $value->nid;
    }
    foreach ($resultTerms as $key => $value) {
      $tids[] = $value->tid;
    }

    $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple($nids);
    $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadMultiple($tids);
    $currentDate = date("F Y");

    $entities = array_merge($nodes, $terms);
    shuffle($output);
    return [
      'entities' => $entities,
      'date' => $currentDate,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
