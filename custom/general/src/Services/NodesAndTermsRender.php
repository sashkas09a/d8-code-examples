<?php

namespace Drupal\general\Services;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Describes methods used to render terms and nodes.
 */
class NodesAndTermsRender {

  /**
   * Database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  public $database;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  public $entityTypeManager;

  /**
   * NodesAndTermsRender constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type service.
   */
  public function __construct(Connection $database, EntityTypeManagerInterface $entityTypeManager) {
    $this->database = $database;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Prepare array with rendered terms and nodes.
   *
   * @param mixed $tid
   *   Tid of parrent taxonomy term. (string or int)
   * @param mixed $page
   *   Current page. (string or int)
   *
   * @return array
   *   Array of rendered taxonomy and nodes items.
   */
  public function outputPrepare($tid, $page = 0) {
    $child_tids = [];
    $child_tid = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree('primary_categories', $tid);
    foreach ($child_tid as $key => $value) {
      $tids[] = $value->tid;
      $child_tids[] = $value->tid;
    }
    $tids[] = $tid;
    $query = $this->database->select('node__field_a_primary_category', 'pc');
    $query->fields('pc', ['entity_id']);
    $query->condition('pc.field_a_primary_category_target_id', $tids, 'IN');
    $query->condition('pc.bundle', 'article');
    $nids = array_unique($query->execute()->fetchCol());

    $count = count(array_merge($nids, $child_tids));
    $remainder = $count % 6;
    $sets = intdiv($count, 6);
    $sets = $remainder > 0 ? ++$sets : $sets;
    $prepared_array = $this->arrayPrepare($nids, $child_tids, $sets);
    $render = $this->renderContent($prepared_array[$page], 'teaser');
    shuffle($render);
    return $render;
  }

  /**
   * Prepare array of sets with terms and nodes IDs.
   *
   * @param array $nids
   *   Node IDs.
   * @param array $tids
   *   Term IDs.
   * @param int $sets
   *   Quantity of available sets.
   *
   * @return array
   *   Array of sets with terms and nodes IDs.
   */
  public function arrayPrepare(array $nids, array $tids, $sets) {
    $prepared_array = [];
    for ($i = 0; $i < $sets; $i++) {
      $nodes_array = [];
      $tids_array = [];
      $count_results = count($nids);
      $count_tids = count($tids);
      // Occurs when there are enough $nids and $tids.
      if ($count_results >= 4 && $count_tids >= 2) {
        for ($n = 0; $n < 4; $n++) {
          $nodes_array['nid' . $n] = $nids[$n];
          unset($nids[$n]);
        }
        for ($t = 0; $t < 2; $t++) {
          $tids_array['tid' . $t] = $tids[$t];
          unset($tids[$t]);
        }
      }
      // Occurs when enough $tids and not enough $nids.
      elseif ($count_results < 4 && $count_tids >= 6 - $count_results) {
        for ($n = 0; $n < $count_results; $n++) {
          $nodes_array['nid' . $n] = $nids[$n];
          unset($nids[$n]);
        }
        for ($t = 0; $t < 6 - $count_results; $t++) {
          $tids_array['tid' . $t] = $tids[$t];
          unset($tids[$t]);
        }
      }
      // Occurs when enough $nids and not enough $tids.
      elseif ($count_tids < 2 && $count_results >= 6 - $count_tids) {
        for ($t = 0; $t < $count_tids; $t++) {
          $tids_array['tid' . $t] = $tids[$t];
          unset($tids[$t]);
        }
        for ($n = 0; $n < 6 - $count_tids; $n++) {
          $nodes_array['nid' . $n] = $nids[$n];
          unset($nids[$n]);
        }
      }
      // Occurs when not enough $nids and $tids.
      elseif ($count_results < 4 || $count_tids < 2) {
        for ($n = 0; $n < $count_results; $n++) {
          $nodes_array['nid' . $n] = $nids[$n];
        }
        for ($t = 0; $t < $count_tids; $t++) {
          $tids_array['tid' . $t] = $tids[$t];
        }
      }
      $prepared_array[$i] = array_merge($nodes_array, $tids_array);
      $nids = array_values($nids);
      $tids = array_values($tids);
    }
    return $prepared_array;
  }

  /**
   * Prepare mixed with rendered terms and nodes.
   *
   * @param array $array
   *   Array from of available sets.
   * @param string $view_mode
   *   Machine name of view_mode.
   *
   * @return array
   *   Array of 6 mixed items (terms and nodes).
   */
  public function renderContent(array $array, $view_mode) {
    $render = [];
    $entityTypeManager = $this->entityTypeManager;
    foreach ($array as $key => $item) {
      if (strpos($key, 'nid') !== FALSE) {
        $node = $entityTypeManager->getStorage('node')->load($item);
        $node_builder = $entityTypeManager->getViewBuilder('node');
        $node_build = $node_builder->view($node, $view_mode);
        $render[] = render($node_build);
      }
      else {
        $term = $entityTypeManager->getStorage('taxonomy_term')->load($item);
        $taxonomy_builder = $entityTypeManager->getViewBuilder('taxonomy_term');
        $taxonomy_build = $taxonomy_builder->view($term, $view_mode);
        $render[] = render($taxonomy_build);
      }
    }
    return $render;
  }

}
