<?php

/**
 * @file
 * Attaches tracker script to Drupal pages.
 */

use Drupal\Core\Url;
use Drupal\Core\Render\Markup;

/**
 * Implements hook_page_attachments().
 */
function tracker_page_attachments(&$attachments) {
  $config = Drupal::config('tracker.settings');
  $path = Drupal::service('path.current')->getPath();
  $user = Drupal::currentUser()->getAccount();
  $status = $config->get('status');

  if (!$status || !tracker_check_user_role($config, $user) || !tracker_check_path($config, $path)) {
    return;
  }
  $node = Drupal::routeMatch()->getParameter('node');
  $nid = $node ? $node->id() : '';
  $uid = $user->id();
  $url = new Url("internal:" . $path);
  $entityType = $url->isRouted() ? array_keys($url->getRouteParameters())[0] : '';
  $scripts = tracker_tracker_scripts($nid, $uid, $entityType);

  foreach ($scripts as $script) {
    $attachments['#attached']['html_head'][] = $script;
  }
}

/**
 * Check that tracker available for current user role.
 *
 * @param \Drupal\Core\Config\ImmutableConfig $config
 *   Object with config data.
 * @param \Drupal\Core\Session\AccountInterface $user
 *   User object.
 *
 * @return bool
 *   TRUE or FALSE.
 */
function tracker_check_user_role(ImmutableConfig $config, AccountInterface $user) {
  $userRole = $user->getRoles();
  $roles = $config->get('analytics_role_list');
  $toggle = $config->get('analytics_role_toggle');
  $bool = count(array_intersect($roles, $userRole)) > 0 ? TRUE : FALSE;
  return tracker_user_and_path_check($bool, $toggle);
}

/**
 * Check that tracker available for current path.
 *
 * @param \Drupal\Core\Config\ImmutableConfig $config
 *   Object with config data.
 * @param string $path
 *   Path to current page.
 *
 * @return bool
 *   TRUE or FALSE.
 */
function tracker_check_path(ImmutableConfig $config, $path) {
  $alias = trim(Drupal::service('path.alias_manager')->getAliasByPath($path), '/');
  $toggle = $config->get('analytics_path_toggle');
  $patterns = $config->get('analytics_path_list');
  $pathMatches = Drupal::service('path.matcher')->matchPath($alias, $patterns);
  return tracker_user_and_path_check($pathMatches, $toggle);
}

/**
 * Check negative and positive condition for current user and current path.
 *
 * @param bool $bool
 *   TRUE or FALSE.
 * @param bool $toggle
 *   Could be '0' or '1'.
 *
 * @return bool
 *   TRUE or FALSE.
 */
function tracker_user_and_path_check($bool, $toggle) {
  return $toggle ? $bool : !$bool;
}

/**
 * Settings for Tracker script.
 *
 * @param int $nid
 *   Current node id.
 * @param int $uid
 *   Current user id.
 * @param string $entityType
 *   Type of entity.
 *
 * @return array
 *   Array of scripts.
 */
function tracker_tracker_scripts($nid, $uid, $entityType) {
  $settings = "Tracker.init();\n";
  $settings .= $nid ? "Tracker.nid = '$nid';\n" : '';
  $settings .= "Tracker.uid = '$uid';\n";
  $settings .= $entityType ? "Tracker.entityname = '$entityType';\n" : '';
  $settings .= "Tracker.sendData();";

  $markup = new Markup();
  $str = $markup->create($settings);
  $script[] = [
    [
      '#attributes' => [
        'src' => 'https://stats.trdjfd#5434812.co/tracker.js',
      ],
      '#tag' => 'script',
      '#weight' => -1,
    ],
    'tracker',
  ];
  $script[] = [
    [
      '#tag' => 'script',
      '#value' => $str,
      '#weight' => 30,
    ],
    'tracker_settings',
  ];
  return $script;
}
