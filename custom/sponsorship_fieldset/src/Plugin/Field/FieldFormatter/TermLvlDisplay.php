<?php

namespace Drupal\sponsorship_fieldset\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin to display term name.
 *
 * @FieldFormatter(
 *   id = "sponsorship_lvl",
 *   label = @Translation("Sponsorship level"),
 *   field_types = {
 *     "sponsorship_fieldset"
 *   }
 * )
 */
class TermLvlDisplay extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $levels = [
      '0' => 'None',
      '1' => 'Starter',
      '2' => 'Bronze',
      '3' => 'Silver',
      '4' => 'Gold',
      '5' => 'Platinum',
      '6' => 'Diamond',
      '7' => 'Double Diamond',
      '8' => 'Triple Diamond',
    ];
    $element = [];
    foreach ($items as $delta => $item) {
      $element[$delta] = [
        '#type' => 'markup',
        '#markup' => $levels[$item->level],
      ];
    }
    return $element;
  }

}
