<?php

namespace Drupal\json_parse\Services;

use Drupal\Component\Serialization\Json;
use GuzzleHttp\Client;
use Drupal\bb_json_parse\Interfaces\JsonParserInterface;

/**
 * Helper methods to parse|sort JSON data.
 */
class JsonParse implements JsonParserInterface {

  /**
   * The json serialization service.
   *
   * @var \Drupal\Component\Serialization\Json
   */
  public $jsonSerialize;

  /**
   * The Guzzle HTTP Client service.
   *
   * @var \GuzzleHttp\Client
   */
  public $httpClient;

  /**
   * JsonParse constructor.
   *
   * @param \Drupal\Component\Serialization\Json $jsonSerialize
   *   The json serialization.
   * @param \GuzzleHttp\Client $httpClient
   *   The HTTP Client.
   */
  public function __construct(Json $jsonSerialize, Client $httpClient) {
    $this->jsonSerialize = $jsonSerialize;
    $this->httpClient = $httpClient;
  }

  /**
   * {@inheritdoc}
   */
  public function parse($url) {
    try {
      $response = $this->httpClient->get($url, ['headers' => ['Accept' => 'application/json']]);
      $responseBody = $response->getBody();
      $decoded = Json::decode($responseBody);
      return $decoded;
    }
    catch (\Exception $e) {
      die('Something goes wrong' . $e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function sortByTitle(array $array, $key) {
    usort($array, function ($a, $b) use ($key) {
      return $a[$key] <=> $b[$key];
    });
  }

  /**
   * {@inheritdoc}
   */
  public function sortByRecent(array $array, $key) {
    usort($array, function ($a, $b) use ($key) {
      return $b[$key] <=> $a[$key];
    });
  }

}
