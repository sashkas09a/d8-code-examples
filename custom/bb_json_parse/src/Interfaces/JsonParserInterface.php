<?php

namespace Drupal\bb_json_parse\Interfaces;

/**
 * Defines an interface for JSON parsing purposes.
 */
interface JsonParserInterface {

  /**
   * Parses JSON data.
   *
   * @param string $url
   *   The URL from which data should fetched.
   *
   * @return array|false
   *   Array of nodes or FALSE if URL couldn't be parsed.
   */
  public function parse($url);

  /**
   * Sorts data by title.
   *
   * @param array $array
   *   Array with nodes.
   * @param string $key
   *   Node field on which sorting should be done.
   */
  public function sortByTitle(array $array, $key);

  /**
   * Sorts data by date.
   *
   * @param array $array
   *   Array with nodes.
   * @param string $key
   *   Node field on which sorting should be done.
   */
  public function sortByRecent(array $array, $key);

}
