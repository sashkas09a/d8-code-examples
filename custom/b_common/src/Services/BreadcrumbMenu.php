<?php

namespace Drupal\b_common\Services;

use Drupal\Core\Database\Connection;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

/**
 * Helper service with methods to manipulate with menu items on multilanguage.
 */
class BreadcrumbMenu {

  /**
   * Database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $database;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private $language;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityManager;

  /**
   * The menu manager.
   *
   * @var \Drupal\Core\Menu\MenuLinkManagerInterface
   */
  public $menuManager;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  private $route;

  /**
   * BreadcrumbMenu constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language
   *   The language manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityManager
   *   The entity type manager service.
   * @param \Drupal\Core\Menu\MenuLinkManagerInterface $menuManager
   *   The menu manager service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route
   *   The route match service.
   */
  public function __construct(Connection $database,
    LanguageManagerInterface $language,
    EntityTypeManagerInterface $entityManager,
    MenuLinkManagerInterface $menuManager,
    RouteMatchInterface $route
  ) {
    $this->database = $database;
    $this->language = $language;
    $this->entityManager = $entityManager;
    $this->menuManager = $menuManager;
    $this->route = $route;
  }

  /**
   * Returns name of current route.
   *
   * @return string
   *   Route name.
   */
  private function getCurrentRouteName() {
    return $this->route->getRouteName();
  }

  /**
   * Check if current route node or taxonomy route.
   *
   * @return string
   *   Route name.
   */
  private function checkCurrentRoute() {
    $route_name = $this->getCurrentRouteName();
    $routes = ['entity.node.canonical', 'entity.taxonomy_term.canonical'];
    $key = array_search($route_name, $routes);
    return $routes[$key];
  }

  /**
   * Builds URL from current route.
   *
   * @param string $routeName
   *   Route to build URL.
   *
   * @return string
   *   Url from current route.
   */
  private function getUrlFromRoute($routeName) {
    switch ($routeName) {
      case 'entity.node.canonical':
        $entityType = 'node';
        break;

      case 'entity.taxonomy_term.canonical':
        $entityType = 'taxonomy_term';
        break;
    }
    $entity = $this->route->getParameter($entityType);
    $language = $this->language->getLanguage('en');
    $options = ['language' => $language];
    $url = new Url($routeName, [$entityType => $entity->id()], $options);
    $url = 'internal:' . $url->toString();
    return $url;
  }

  /**
   * Parent menu item uuid for current URL and current menu item id.
   *
   * @param string $url
   *   Route to build URL.
   *
   * @return array
   *   Arr with: ID for current menu item and parent uuid for current menu item.
   */
  private function getParentUuid($url) {
    $query = $this->database->select('menu_link_content_data', 'mlc')
      ->condition('mlc.link__uri', $url, 'IN')
      ->fields('mlc', ['id', 'parent']);
    $result = $query->execute()->fetchAssoc();
    // Change array key to more readable.
    $result = $result ? ['curMenuId' => $result['id'], 'parent' => $result['parent']] : FALSE;
    return $result;
  }

  /**
   * Get 'menu_link_content' for current menu item from current menu item id.
   *
   * @param array $menuItemUuid
   *   Arr with: ID for current menu item and parent uuid for current menu item.
   *
   * @return string
   *   'menu_link_content' uuid.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist ('menu_link_content').
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   */
  private function getCurrentUuid(array $menuItemUuid) {
    $menuItemObj = $this->entityManager->getStorage('menu_link_content')->loadByProperties(['id' => $menuItemUuid['curMenuId']]);
    $menuItemObj = reset($menuItemObj);
    $itemUuid = 'menu_link_content:' . $menuItemObj->uuid();
    return $itemUuid;
  }

  /**
   * Get all parents uuid for current menu item.
   *
   * @param string $currentItemUuid
   *   UUID for current menu item.
   * @param array $menuItemUuid
   *   Array with current menu item ID and parent uuid for current menu item.
   *
   * @return array
   *   All parents uuid for current menu item if parents exist,
   *   + uuid for current menu item.
   */
  private function getAllParentsUuid($currentItemUuid, array $menuItemUuid) {
    $parents = $this->menuManager->getParentIds($menuItemUuid['parent']);
    $firstElem[$currentItemUuid] = $currentItemUuid;
    $parents = $parents ? $firstElem + $parents : $firstElem;
    return $parents;
  }

  /**
   * Get and return 'menu_link_content' definitions for current menu item and it parents.
   *
   * @param string $menu_name
   *   Menu name.
   *
   * @return array
   *   All parents uuid for current menu item if parents exist,
   *   + uuid for current menu item.
   */
  public function mainMenuBuilder($menu_name) {
    if ($menu_name === 'main') {
      $routeName = $this->getCurrentRouteName();
      if ($routeName === $this->checkCurrentRoute()) {
        $url = $this->getUrlFromRoute($routeName);
      }
      if ($url) {
        $menuItemUuid = $this->getParentUuid($url);
        if ($menuItemUuid) {
          $currentUuid = $this->getCurrentUuid($menuItemUuid);
          $parents = $this->getAllParentsUuid($currentUuid, $menuItemUuid);
          return $parents;
        }
      }
    }
    return FALSE;
  }

}
